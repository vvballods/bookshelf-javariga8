public class Journal {
    private String name;

    public Journal(String name) {
        this.name = name;
    }

    public void lookingAtPictures() {
        System.out.println("Looking at pictures from Journal: " + this);
    }

    @Override
    public String toString() {
        return "Journal{" +
                "name='" + name + '\'' +
                '}';
    }
}

class JournalAdapter implements ReadableMaterial {
    private Journal journal;

    public JournalAdapter(Journal journal) {
        this.journal = journal;
    }

    @Override
    public void read() {
        journal.lookingAtPictures();
    }
}
