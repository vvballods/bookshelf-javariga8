enum BookSortType {
    BY_AUTHOR, BY_TITLE, BY_ISBN
}