import java.time.LocalDateTime;
import java.util.Stack;

import static java.time.LocalDateTime.now;

public class BookshelfSizeMemento {
    private Stack<BookshelfSize> history = new Stack<>();

    public void save(Bookshelf bookshelf) {
        history.push(new BookshelfSize(bookshelf.booksCount(), now()));
    }

    public void showHistory() {
        while (history.size() > 0) {
            System.out.println(history.pop());
        }
    }
}

class BookshelfSize{
    int size;
    LocalDateTime date;

    public BookshelfSize(int size, LocalDateTime date) {
        this.size = size;
        this.date = date;
    }

    @Override
    public String toString() {
        return "BookshelfSize{" +
                "size=" + size +
                ", date=" + date +
                '}';
    }
}
