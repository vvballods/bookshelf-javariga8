public class Human {
    private ReadableMaterial material;

    public Human(ReadableMaterial material) {
        this.material = material;
    }

    void read() {
        material.read();
    }
}
