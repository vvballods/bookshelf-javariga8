public class Book implements ReadableMaterial {
    private String isbn;
    private String title;
    private String author;
    private int pages;
    private boolean read;

    private Book(Builder builder) {
        this.isbn = builder.isbn;
        this.title = builder.title;
        this.author = builder.author;
        this.pages = builder.pages;
        this.read = builder.read;
    }

    @Override
    public void read() {
        System.out.println("Reading carefully a book: " + this);
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getPages() {
        return pages;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    @Override
    public String toString() {
        return "Book{" +
                "isbn='" + isbn + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", pages=" + pages +
                ", read=" + read +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        return isbn.equals(book.isbn);
    }

    @Override
    public int hashCode() {
        return isbn.hashCode();
    }

    public static class Builder {
        private String isbn;
        private String title;
        private String author;
        private int pages;
        private boolean read;

        public Builder(String isbn) {
            this.isbn = isbn;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setAuthor(String author) {
            this.author = author;
            return this;
        }

        public Builder setPages(int pages) {
            this.pages = pages;
            return this;
        }

        public Builder setRead(boolean read) {
            this.read = read;
            return this;
        }

        Book build() {
            if (isbn.length() > 8) {
                return new Book(this);
            }
            throw new IllegalArgumentException("Nepareizs ISBN");
        }
    }
}
