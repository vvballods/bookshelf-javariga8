import java.util.List;

public interface Bookshelf {
    List<Book> getAllBooks();
    List<Book> searchBook(String query);
    void addBook(Book book);
    void removeBook(Book book);
    void readBook(Book book);
    void sortBooks(BookSortType sort);
    int booksCount();
}
