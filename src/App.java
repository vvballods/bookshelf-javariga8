import java.util.concurrent.TimeUnit;

public class App {

    public static void main(String[] args) {
        Book book = new Book.Builder("1234567890")
                .setAuthor("Valters & Rapa")
                .setTitle("Reps & Dzīve")
                .setPages(123)
                .setRead(false)
                .build();

        Journal santa = new Journal("Santa");
        JournalAdapter journal = new JournalAdapter(santa);

        book.read();
        journal.read();

        BookCreationProcess bookCreationProcess = new BookCreationProcess();
        bookCreationProcess.startProcess(book);

        Book book2 = new Book.Builder("0987654321")
                .setAuthor("Cita dzīve")
                .setTitle("Reps & Dzīve")
                .setPages(457)
                .setRead(false)
                .build();

        Bookshelf bookshelf = new MyBookshelf();
        bookshelf.addBook(book);
        System.out.println(bookshelf.getAllBooks());
        bookshelf.readBook(book);
        System.out.println(bookshelf.getAllBooks());

        BookshelfSizeMemento memento = new BookshelfSizeMemento();
        memento.save(bookshelf);

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        bookshelf.addBook(book2);
        memento.save(bookshelf);

        memento.showHistory();
    }
}
