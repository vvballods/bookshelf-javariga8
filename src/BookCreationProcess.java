// jauna grāmata -> izdevēja -> ražotnē -> grāmatnīcā -> grāmatu plauktā
public class BookCreationProcess {
    BookCreationStep process;

    public BookCreationProcess() {
        process = new BookCreationStep("Jauna grāmata",
                new BookCreationStep("Izdevējs",
                        new BookCreationStep("Ražotnē",
                                new BookCreationStep("Grāmatīcā",
                                        new BookCreationStep("Grāmatu plauktā")))));
    }

    public void startProcess(Book book) {
        process.execute(book);
    }
}

class BookCreationStep {
    String stepName;
    BookCreationStep next;

    public BookCreationStep(String stepName) {
        this.stepName = stepName;
        this.next = null;
    }

    public BookCreationStep(String stepName, BookCreationStep next) {
        this.stepName = stepName;
        this.next = next;
    }

    public void execute(Book book) {
        System.out.println("Executing " + stepName + " for book " + book);
        if (next != null) {
            next.execute(book);
        }
    }
}
