import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.emptyList;

public class MyBookshelf implements Bookshelf {
    List<Book> books = new ArrayList<>();

    @Override
    public List<Book> getAllBooks() {
        return books;
    }

    @Override
    public List<Book> searchBook(String query) {
        // TODO will do later
        return emptyList();
    }

    @Override
    public void addBook(Book book) {
        books.add(book);
    }

    @Override
    public void removeBook(Book book) {
        if (books.contains(book)) {
            books.remove(book);
        }
    }

    @Override
    public void readBook(Book book) {
        if (books.contains(book)) {
            book.setRead(true);
        } else {
            System.out.println("Book is not in a bookshelf!");
        }
    }

    @Override
    public void sortBooks(BookSortType sort) {
        // TODO will do later
    }

    @Override
    public int booksCount() {
        return books.size();
    }
}
